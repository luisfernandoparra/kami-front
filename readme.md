# Kami Frontend
A font server to publish campaign assets, manage pixels and redirects.

Developed using Lumen Micro-framework 5.5
if you need to check exact framework version run `php artisan -V`

## Requisites
* PHP >= 7.0.0

Php Modules
* Curl
* Json
* OpenSSL
* PDO
* Mbstring
* Tokenizer
* bcmath

## First Deploy
1. Setup deploy ssh keys at gitlab project configuration.
2. [Install composer](https://getcomposer.org/download/) and [Setup globally](https://getcomposer.org/doc/00-intro.md#globally)
3. `git clone git@dev.fasttracknet.es:netsales/kami/front.git kami-front`
4. `cd kami-front`
5. `cp .env.example .env` and setup .env with correct values. (to generate APP_KEY read appendix I and take care about queue notes)
6. `composer install --no-dev`

Remember, you need to connect kami webapp and kami front projects. At this moment this connection can be done using ftp or local drivers at kami-webapp configuration. As kami is using laravel and indeed is using flysystem is really easy to add new config drivers.

## Queue
Please, take special consideration to queue section. You must use `QUEUE_DRIVER=database` at .env file but **NO** configure supervisor to run jobs.
All the jobs must be processed by Kami main project.

## Kami Key
One of the .env keys is the KAMI_KEY, this must match with main Kami project random key string. If not, unsubscribes will not work.

## Appendix I
using bash, this command will generate 32 random characteres

```bash
for a in `seq 1 10000`; do echo -n `date +%s%N`${RANDOM} | sha1sum - ;done | md5sum - | cut -d" " -f1
```

Will take a while (at local i3 server take ~40 seconds)
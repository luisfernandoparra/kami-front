<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return redirect('https://kami.netsales.es');
});

$router->get('/link/{campaign}/{database}/{user}/{position}', ['uses' => 'KamiController@link']);
$router->get('/mirror/{campaign}/{database}/{user}', ['uses' => 'KamiController@mirror']);
$router->get('/px/{campaign}/{database}/{user}', ['uses' => 'KamiController@pixel']);
$router->get('/pr/{campaign}/{database}/{user}/{position}', ['uses' => 'KamiController@proxy']);
$router->get('/un/{campaign}/{database}/{user}/{checksum}', ['uses' => 'KamiController@unsubscribe']);
$router->get('/sp/{campaign}/{database}/{user}/{checksum}', ['uses' => 'KamiController@spam']);
//$router->get('/url/{campaign}/{database}/{user}', ['uses' => 'KamiController@proxyToUrl']);

//$router->get('/preview/mirror/{creative}/{database}/', ['uses' => 'PreviewController@mirror']);

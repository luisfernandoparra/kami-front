<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Lamentamos que te vayas</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        body {
            padding: 0px;
            margin: 0px;
            background-color: rgb(173, 173, 173);
        }

        div.content {
            padding: 20px;
            color: rgb(90, 90, 90);
            background-color: white;
            margin-top: 50px;
            height: 70vh;
        }

        h1 {
            font-size: 2em;
            padding-bottom: 30px;
        }

        div.content p {
            font-size: 1.2em;
        }

        .label,
        .badge {
            border:1px solid #b5b5b5;
            padding: 3px 3px;
            margin: 3px 3px;
            text-align: center;
            vertical-align: baseline;
            white-space: nowrap;
        }

        .label {
            border-radius: 4px;
            display: inline;
        }

        .badge {
            border-radius: 10px;
            display: inline-block;
            padding: 1px 7px;
        }

        .label:empty,
        .badge:empty {
            display: none;
        }
    </style>
</head>

<body>
    <div></div>
    <div class="content">
        <h1>Lamentamos que te vayas</h1>
        <p>Hemos guardado tu petición y será procesada de forma automatizada lo antes posible.</p>
        <p>Tu baja en {{ $database }}
            @if(!empty($email)) para el correo <span class="label">{{ $email }}</span>@endif
            será efectiva en menos de una semana. Mientras tanto, es posible que recibas algun correo.</p>
    </div>
</body>

</html>
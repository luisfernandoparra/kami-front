<?php

namespace App\Settings;

interface SettingsResolverInterface {

    public function path($uuid);

}
<?php

namespace App\Settings;

use App\Settings\SettingsResolverInterface;

class CreativePreviewSettingsResolver implements SettingsResolverInterface
{

    public function path($uuid)
    {

        // we use md5 to make homogeneous distribution
        $hash = md5($uuid);
        // take care about this, too many files at same directory make memory issues and access latency
        // recomended distributions at x/yy/file is used here.
        $subPath = substr($hash, 0, 1) . DIRECTORY_SEPARATOR . substr($hash, 1, 2) . DIRECTORY_SEPARATOR . $uuid;

        return storage_path("app/creative/".$subPath).DIRECTORY_SEPARATOR;
    }
}
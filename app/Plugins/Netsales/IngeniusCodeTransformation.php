<?php

namespace App\Plugins\Netsales;

use DB;
use App\Campaign\Campaign;
use App\Plugins\PluginInterface;
use Cache;
use App\Plugins\LinkTransformerInterface;

class IngeniusCodeTransformation implements LinkTransformerInterface, PluginInterface {

    /**
     * Campaign loaded from url ids
     *
     * @var Campaign
     */
    protected $campaign;

    /**
     * List of database uuid and ingenius codes
     *
     * @var Array
     */
    protected $ingenius_codes;

    public function __construct(Campaign $campaign){
        $this->campaign = $campaign;
    }

    public function link($link, $database_uuid)
    {
        $ingenius_codes = Cache::remember('db:ingenius_codes', 120, function () {
            $rawCodes = DB::table('ingenius_codes')->select("databases.uuid", "ingenius_codes.code")->join('databases', 'ingenius_codes.database_id', '=', 'databases.id')->get();
            $codes = [];
            foreach($rawCodes as $rawCode){
                $codes[$rawCode->uuid] = $rawCode->code;
            }
            return $codes;
        });

        if(is_null($ingenius_codes) || !is_array($ingenius_codes) || !array_key_exists($database_uuid, $ingenius_codes)){
            return $link;
        }
        $code = $ingenius_codes[$database_uuid];

        $re = '/amc=email\.netsales\.([0-9]+)\.([0-9]+)\.([0-9]+)/';
        $subst = 'amc=email.netsales.\1.'.$code.'.\3';

        $link = preg_replace($re, $subst, $link);
        return $link;
    }
}
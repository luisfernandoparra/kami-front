<?php

namespace App\Plugins;

interface LinkTransformerInterface {
    public function link($link, $database_uuid);
}
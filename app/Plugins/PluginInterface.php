<?php

namespace App\Plugins;

use App\Campaign\Campaign;

interface PluginInterface {
    public function __construct(Campaign $campaign);
}
<?php

namespace App\Campaign\Exceptions;

use App\Settings\SettingsResolverInterface;


class FooterNotFound extends \Exception
{
    protected $message = 'Footer not found';
}
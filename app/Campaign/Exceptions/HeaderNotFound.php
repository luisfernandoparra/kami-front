<?php

namespace App\Campaign\Exceptions;

use App\Settings\SettingsResolverInterface;


class HeaderNotFound extends \Exception
{
    protected $message = 'Header not found';
}
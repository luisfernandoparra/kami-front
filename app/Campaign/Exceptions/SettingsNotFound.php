<?php

namespace App\Campaign\Exceptions;

use App\Settings\SettingsResolverInterface;


class SettingsNotFound extends \Exception
{
    protected $message = 'config file not found';
}
<?php

namespace App\Campaign;

use Log;
use App\Settings\SettingsResolverInterface;
use App\Campaign\Exceptions\SettingsNotFound;
use App\Campaign\Exceptions\HeaderNotFound;
use App\Campaign\Exceptions\FooterNotFound;
use App\Plugins\PluginInterface;


class Campaign
{
    protected $campaignUuid;
    protected $settings;
    protected $settingsPath;
    protected $settingsFile;
    protected $templateFile;

    public function __construct($uuid, SettingsResolverInterface $settingsResolver){
        $this->campaignUuid = $uuid;
        $this->settingsPath = $settingsResolver->path($uuid);
        $this->settingsFile = $this->settingsPath."config.json";
        $this->templateFile = $this->settingsPath."template.html";
        if(!file_exists($this->settingsFile)){
            throw new SettingsNotFound('config file not found at '.$this->settingsFile);
        }
        try {
            $this->settings = json_decode(file_get_contents($this->settingsFile));
        } catch(\Throawble $t) {
            Log::critical("Cannot read config file but file exists at '".$this->settingsFile."'");
            throw $t;
        }
    }

    public function getLink($position)
    {
        $link = false;
        if(!empty($this->settings)){
            if(isset($this->settings->links) && array_key_exists($position, $this->settings->links)){
                $link = $this->settings->links[$position];
            }
        }
        return $link;
    }

    public function getPixelForProxy($position)
    {
        $pixel = false;
        if(!empty($this->settings)){
            if(isset($this->settings->pixels) && array_key_exists($position, $this->settings->pixels)){
                $pixel = $this->settings->pixels[$position];
            }
        }
        return $pixel;
    }

    public function getTemplate($databaseUuid, $replacements)
    {

        if(!file_exists($this->settingsPath.$databaseUuid."_header.tpl")){
            throw new HeaderNotFound();
        }
        if(!file_exists($this->settingsPath.$databaseUuid."_footer.tpl")){
            throw new FooterNotFound();
        }

        $body = file_get_contents($this->templateFile);
        $header = file_get_contents($this->settingsPath.$databaseUuid."_header.tpl");
        $footer = file_get_contents($this->settingsPath.$databaseUuid."_footer.tpl");
        $template = str_replace(['{{header}}', '{{footer}}','{{preview-text}}'], [$header, $footer,''], $body);

        if(!is_array($replacements)){
            $replacements = [];
        }
        return str_replace(array_keys($replacements), array_values($replacements), $template);
    }

    public function getCampaignUuid()
    {
        return $this->campaignUuid;
    }

    public function getCampaignId()
    {
        $id = false;
        if(!empty($this->settings)){
            if(isset($this->settings->campaign_id)){
                $id = $this->settings->campaign_id;
            }
        }
        return $id;
    }

    public function getCreativeId()
    {
        $id = false;
        if(!empty($this->settings)){
            if(isset($this->settings->creative_id)){
                $id = $this->settings->creative_id;
            }
        }
        return $id;
    }

    public function getSegmentId()
    {
        $id = false;
        if(!empty($this->settings)){
            if(isset($this->settings->segment_id)){
                $id = $this->settings->segment_id;
            }
        }
        return $id;
    }

    public function getBaseDomains()
    {
        if(!isset($this->settings, $this->settings->baseDomains)){
            return [];
        }
        return (array) ($this->settings->baseDomains);
    }

    public function getDatabasePublicName($database)
    {
        $name = "";
        $data = $this->getDatabaseData($database);
        if($data !== false && isset($data->public_name)){
            $name = $data->public_name;
        }
        return $name;
    }

    public function getDatabaseId($database)
    {
        $id = "";
        $data = $this->getDatabaseData($database);
        if($data !== false && isset($data->id)){
            $id = $data->id;
        }
        return $id;
    }

    public function getDatabaseListUnsubscribeAddress($database)
    {
        $listUnsubscribeAddress = "";
        $data = $this->getDatabaseData($database);
        if($data !== false && isset($data->list_unsubscribe_address)){
            $listUnsubscribeAddress = $data->list_unsubscribe_address;
        }
        return $listUnsubscribeAddress;
    }

    protected function getDatabaseData($database){
        $data = false;
        if(!empty($this->settings)){
            if(isset($this->settings->databases, $this->settings->databases->{$database})){
                $data = $this->settings->databases->$database;
            }
        }
        return $data;
    }

    public function getPlugins(){
        $plugins = null;
        if(isset($this->settings, $this->settings->plugins) && is_array($this->settings->plugins)){
            $plugins = [];
            foreach($this->settings->plugins as $pluginPath){
                $next = null;
                try {
                    $className = "\\App\\Plugins\\".$pluginPath; 
                    $next = new $className($this);
                } catch(\Throwable $t){
                    Log::critical('Cannot create plugin ('.$pluginPath.') instance');
                    throw $t;
                }
                if(!is_null($next) && $next instanceof PluginInterface){
                    $plugins[] = $next;
                }
            }
        }
        return $plugins;
    }
}
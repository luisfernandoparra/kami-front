<?php

namespace Kami\Jobs;

use App\Jobs\Job;
use Carbon\Carbon;

class UserClickJob extends Job
{
    public $queue = "stats";

    protected $campaign;
    protected $database;
    protected $user;
    protected $position;
    protected $ip;
    protected $userAgent;
    protected $remote_database;
    protected $when;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $database, $user, $position, $ip, $userAgent, Carbon $when, $remote_database=null)
    {
        $this->campaign = $campaign;
        $this->database = $database;
        $this->user = $user;
        $this->position = $position;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->remote_database = $remote_database;
        $this->when = $when;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        throw new \LogicException("This task must run only at main Kami project");
    }
}

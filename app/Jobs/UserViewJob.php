<?php

namespace Kami\Jobs;
use App\Jobs\Job;

class UserViewJob extends Job
{
    public $queue = "stats";

    protected $campaign;
    protected $database;
    protected $user;
    protected $ip;
    protected $userAgent;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $database, $user, $ip, $userAgent)
    {
        $this->campaign = $campaign;
        $this->database = $database;
        $this->user = $user;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        throw new \LogicException("This task must run only at main Kami project");
    }
}

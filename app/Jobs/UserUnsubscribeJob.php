<?php

namespace Kami\Jobs;
use App\Jobs\Job;

class UserUnsubscribeJob extends Job
{
    public $queue = "stats";

    protected $campaign;
    protected $database;
    protected $user;
    protected $ip;
    protected $userAgent;
    protected $queryValues;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($campaign, $database, $user, $ip, $userAgent, $queryValues)
    {
        $this->campaign = $campaign;
        $this->database = $database;
        $this->user = $user;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->queryValues = $queryValues;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        throw new \LogicException("This task must run only at main Kami project");
    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Campaign\Campaign;
use Kami\Jobs\UserViewJob;
use Illuminate\Support\Str;
use Kami\Jobs\UserClickJob;
use Illuminate\Http\Request;
use Kami\Jobs\UserMirrorOpenJob;
use Kami\Jobs\UserUnsubscribeJob;
use App\Plugins\LinkTransformerInterface;
use App\Settings\CampaignSettingsResolver;
use App\Campaign\Exceptions\HeaderNotFound;
use App\Campaign\Exceptions\SettingsNotFound;

class KamiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CampaignSettingsResolver $settingsResolver)
    {
        $this->settingsResolver = $settingsResolver;
    }

    public function link(Request $request, $campaign, $database, $user, $position)
    {
        // sanitize
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $campaign) ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $database) ||
            !preg_match('/^[0-9]+$/', $user) ||
            !preg_match('/^[0-9]+$/', $position)) {
            return abort(404);
        }
        // end sanitize
        try {
            $settings = new Campaign($campaign, $this->settingsResolver);
        } catch (SettingsNotFound $e) {
            abort(404, "Not found");
        } catch (\Throwable $t) {
            throw $t;
        }

        $link = $settings->getLink($position);

        if (empty($link)) {
            abort(404, "Not found");
        }

        $plugins = $settings->getPlugins();
        if (!is_null($plugins) && is_array($plugins)) {
            foreach ($plugins as $plugin) {
                if ($plugin instanceof LinkTransformerInterface) {
                    $link = $plugin->link($link, $database);
                }
            }
        }
        
        // at this moment, this variables are not filled, creative publish must add this values to links if needed.
        $userName = preg_replace('/[^[:alnum:][:space:]\-\.]/ui', '', (base64_decode($request->get('unb', ''))));
        $cp = preg_replace('/[^[:alnum:][:space:]\-\.]/ui', '', base64_decode($request->get('ucpb', '')));
        $email = filter_var(base64_decode($request->get('ueb', '')), FILTER_VALIDATE_EMAIL);

        $replacements = [
            '[segment_id]' => $settings->getSegmentId(),
            '[campaign_uuid]' => $campaign,
            '[campaign_id]' => $settings->getCampaignId(),
            '[database_uuid]' => $database,
            '[database_id]' => $settings->getDatabaseId($database),
            '[user_id]' => $user,
            '[user_name]' => urlencode($userName),
            '[user_cp]' => urlencode($cp),
            '[user_email]' => urlencode($email),
            '[random]' => time(),
        ];

        $link = str_replace(array_keys($replacements), array_values($replacements), $link);
        
        $ip = $request->ip();
        $userAgent = $request->header('User-Agent');
        $remoteDatabase = $request->get('urd', "");
        dispatch(new UserClickJob($campaign, $database, (int)$user, (int)$position, $ip, $userAgent, Carbon::now("UTC"), $remoteDatabase));

        return redirect($link, 302);
    }

    public function proxy($campaign, $database, $user, $position)
    {
        // sanitize
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $campaign) ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $database) ||
            !preg_match('/^[0-9]+$/', $user) ||
            !preg_match('/^[0-9]+$/', $position)) {
            return abort(404);
        }
        // end sanitize

        try {
            $settings = new Campaign($creative, $this->settingsResolver);
        } catch (SettingsNotFound $e) {
            abort(404, "Not found");
        } catch (\Throwable $t) {
            throw $t;
        }
        $link = $settings->getPixelForProxy($position);
        if (empty($link)) {
            abort(404, "Not found");
        }

        $plugins = $settings->getPlugins();
        if (!is_null($plugins) && is_array($plugins)) {
            foreach ($plugins as $plugin) {
                if ($plugin instanceof LinkTransformerInterface) {
                    $link = $plugin->link($link, $database);
                }
            }
        }

        return redirect($link, 302);
    }

    public function mirror(Request $request, $campaign, $database, $user)
    {
        // sanitize
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $campaign) ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $database) ||
            !preg_match('/^[0-9]+$/', $user)) {
            return abort(404);
        }
        // end sanitize
        try {
            $settings = new Campaign($campaign, $this->settingsResolver);
        } catch (SettingsNotFound $e) {
            abort(404, "Not found");
        } catch (\Throwable $t) {
            throw $t;
        }

        $baseDomains = $settings->getBaseDomains();
        if (isset($baseDomains[$request->getHost()])) {
            $domainBase = $baseDomains[$request->getHost()];
        } else {
            abort(404);
        }

        $key = env('KAMI_KEY');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        $userNameComma = "";
        $userName = preg_replace('/[^[:alnum:][:space:]\-\.]/ui', '', (base64_decode($request->get('unb', ''))));
        if ($userName != "") {
            $userNameComma = $userName . ",";
        }

        $userSurname = preg_replace('/[^[:alnum:][:space:]\-\.]/ui', '', (base64_decode($request->get('usb', ''))));

        $cp = preg_replace('/[^[:alnum:][:space:]\-\.]/ui', '', base64_decode($request->get('ucpb', '')));

        $email = filter_var(base64_decode($request->get('ueb', '')), FILTER_VALIDATE_EMAIL);

        $remoteDatabase = preg_replace('/[^[:alnum:]\-]/ui', '', $request->get('urd', ''));

        $unsubXtraField = preg_replace('/[^[:alnum:]\-\.\|_:\/]/ui', '', $request->get('uuxf', ''));

// tienes que recoger las variables del unsub xtra field
// y pasarlas de nuevo al enlace


        try {
            $outputPage = $settings->getTemplate($database, [
                '[domain]' => $request->getHost(),
                '[domain_base]' => $domainBase,
                '[image_folder]' => "assets/" . $this->getSubpath($campaign),
                '[campaign_id]' => $settings->getCampaignId(),
                '[campaign_uuid]' => $campaign,
                '[database_id]' => $settings->getDatabaseId($database),
                '[database_uuid]' => $database,
                '[unsub_checksum]' => sha1($user . $campaign . $key),
                '[user_id]' => $user,
                '[user_name]' => $userName,
                '[user_surname]' => $userSurname,
                '[user_name_base64]' => base64_encode($userName), // re-encode to avoid html/xss inyection
                '[user_surname_base64]' => base64_encode($userSurname), // re-encode to avoid html/xss inyection
                '[user_name_with_comma]' => $userNameComma,
                '[user_cp]' => $cp,
                '[user_cp_base64]' => base64_encode($cp), // re-encode to avoid html/xss inyection
                '[user_email]' => $email,
                '[user_email_base64]' => base64_encode($email), // re-encode to avoid html/xss inyection
                '[database_public_name]' => $settings->getDatabasePublicName($database),
                '[database_list_unsubscribe_address]' => $settings->getDatabaseListUnsubscribeAddress($database),
                '[random]' => time(),
                '[user_remote_database]' => $remoteDatabase,
                '[unsub_extra_field_parameter]' => $unsubXtraField,
            ]);
            $ip = $request->ip();
            $userAgent = $request->header('User-Agent');
            dispatch(new UserMirrorOpenJob($campaign, $database, (int)$user, $ip, $userAgent));
            return $outputPage;
        } catch (HeaderNotFound $e) {
            return abort(404);
        } catch (FooterNotFound $e) {
            return abort(404);
        }
    }

    public function pixel(Request $request, $campaign, $database, $user)
    {
        // sanitize
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $campaign) ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $database) ||
            !preg_match('/^[0-9]+$/', $user)) {
            return abort(404);
        }
        // end sanitize
        $ip = $request->ip();
        $userAgent = $request->header('User-Agent');

        dispatch(new UserViewJob($campaign, $database, (int)$user, $ip, $userAgent));
        return response(base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw=='))
            ->header('Content-Type', 'image/gif');
    }

    public function unsubscribe(Request $request, $campaign, $database, $user, $checksum)
    {
        // sanitize
        if (!preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $campaign) ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/', $database) ||
            !preg_match('/^[0-9]+$/', $user)) {
            return abort(404);
        }

        try {
            $settings = new Campaign($campaign, $this->settingsResolver);
        } catch (SettingsNotFound $e) {
            abort(404, "Not found");
        } catch (\Throwable $t) {
            throw $t;
        }

        $baseDomains = $settings->getBaseDomains();
        if (isset($baseDomains[$request->getHost()])) {
            $domainBase = $baseDomains[$request->getHost()];
        } else {
            abort(404);
        }

        $key = env('KAMI_KEY');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        if (sha1($user . $campaign . $key) !== $checksum) {
            return abort(422);
        }
        // end sanitize
        $ip = $request->ip();
        $userAgent = $request->header('User-Agent');
        $query = $request->all();
        dispatch(new UserUnsubscribeJob($campaign, $database, (int)$user, $ip, $userAgent, $query));

        $email = filter_var(base64_decode($request->get('ueb', '')), FILTER_VALIDATE_EMAIL);

        return view('unsubscribe', ['email' => $email, 'database' => $settings->getDatabasePublicName($database)]);
    }

    protected function getSubpath($id)
    {
        // we use md5 to make homogeneous distribution
        $hash = md5($id);
        // take care about this, too many files at same directory make memory issues and access latency
        // recomended distributions at x/yy/file is used here.
        return substr($hash, 0, 1) . DIRECTORY_SEPARATOR . substr($hash, 1, 2) . DIRECTORY_SEPARATOR . $id;
    }
}

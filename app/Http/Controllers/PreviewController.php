<?php

namespace App\Http\Controllers;

use App\Campaign\Campaign;
use Illuminate\Http\Request;
use App\Settings\CreativePreviewSettingsResolver;
use App\Campaign\Exceptions\SettingsNotFound;

class PreviewController extends Controller
{

    public $settingsResolver;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CreativePreviewSettingsResolver $settingsResolver)
    {
        $this->settingsResolver = $settingsResolver;
    }

    public function link($campaign, $database, $position)
    {
        $config = new Campaign($campaign);
    }

    public function proxy($campaign, $database, $position)
    {

    }

    public function mirror(Request $request, $creative, $database)
    {
        try {
            $settings = new Campaign($creative, $this->settingsResolver);
        } catch(SettingsNotFound $e){
            abort(404, "Not found");
        } catch(\Throwable $t){
            throw $t;
        }

        return $settings->getTemplate([
            '[domain]' => $request->getHost(),
            '[image_folder]' => "creative",
            '[campaign_uuid]' => $creative,
            '[campaign_id]' => $settings->getCampaignId(),
            '[database_uuid]' => $database,
            '[user_id]' => 'preview_user',
            ]);
    }

    public function pixel($campaign, $database)
    {

    }

    public function unsubscribe($campaign, $database, $checksum)
    {

    }
}
